# The_Archive

The Archive, project by a & the digdeeper community

If you'd like to request something to be put here please message me on xmpp (archive@jabber.ccc.de) for requests

My OMEMO Fingerprint is : 6031698D 13AA0779 31484C4B 4E36278E 09BA21A6 C825D9E7 3CB0881C D58A4111

here serves as an archive for tools, sites, projects that I find interesting and feel like they need to be preserved

When requesting something to be hosted on here, please use common sense. Nothing violating the TOS will be stored here.

DISCLAIMER: PLEASE USE IN GOOD FAITH AND AT YOUR OWN RISK! ominous cannot warranty the expressions and suggestions of the contents, as well as its accuracy. In addition, to the extent permitted by the law, ominous shall not be responsible for any losses and/or damages due to the usage of the information on our website. By using this repository, you hereby consent to our disclaimer and agree to its terms. The links contained on this repository may lead to external sites, which are provided for convenience only. Any information or statements that appeared in these sites are not sponsored, endorsed, or otherwise approved by ominous. For these external sites, ominous cannot be held liable for the availability of, or the content located on or through it. Plus, any losses or damages occurred from using these contents or the internet generally.
